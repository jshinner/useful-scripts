"""
Script that submits a batch of HTCondor jobs to use the ATLAS AnalysisTop framework
to process a batch of ROOT files.

Takes three command line arguments:
-i/--inputFile: .txt file containing a list of the ROOT files to be processed
-c/--configFile: .txt file containing the config settings for AnalysisTop
-v/--version: The release version of AnalysisTop to use, probably 21.2.X
"""

import os
import sys
import argparse

dirPath = os.path.dirname(os.path.realpath(__file__))


def makeInputFiles(index, line, configFilePath):
    """Create the input.txt and validation-cuts.txt file required for each job."""
    with open(dirPath + "/inputFiles/input%i.txt" % (index), "w") as inputTxtFile:
        inputTxtFile.write(line)
    with open(
        dirPath + "/validation-cuts/validation-cuts%i.txt" % (index), "w"
    ) as validationCutsTxtFile:
        with open(configFilePath, "r") as configFile:
            for configIndex, configFileLine in enumerate(configFile):
                if "OutputFilename" in configFileLine:
                    validationCutsTxtFile.write(
                        "OutputFilename "
                        + dirPath
                        + "/outputFiles/output%i.root\n" % (index)
                    )
                else:
                    validationCutsTxtFile.write(configFileLine)
    return 0


def makeExecutable(index, analysisTopVersion):
    """Create the executable required for each job."""
    with open(
        dirPath + "/executables/AnalysisTopJob%i.sh" % (index), "w"
    ) as executable:
        executable.write("#! /bin/bash\n")
        executable.write(
            "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n"
        )
        executable.write(
            "source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n"
        )
        executable.write("asetup AnalysisBase,%s\n" % analysisTopVersion)
        executable.write(
            "top-xaod "
            + dirPath
            + "/validation-cuts/validation-cuts%i.txt " % index
            + dirPath
            + "/inputFiles/input%i.txt\n" % index
        )
    return 0


def makeCondorScript(index):
    """Make the .condor script required for each job"""
    with open(
        dirPath + "/condorScripts/AnalysisTopJob%i.condor" % (index), "w"
    ) as condorScript:
        condorScript.write("universe = vanilla\n")
        condorScript.write(
            "executable = " + dirPath + "/executables/AnalysisTopJob%i.sh\n" % (index)
        )
        condorScript.write("output = " + dirPath + "/logs/job%i.out\n" % (index))
        condorScript.write("error = " + dirPath + "/logs/job%i.error\n" % (index))
        condorScript.write("queue\n")
    return 0


def submitCondorScript(index):
    """Submit the condor jobs."""
    command = (
        "condor_submit " + dirPath + "/condorScripts/AnalysisTopJob%i.condor" % (index)
    )
    os.system(command)
    return 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--inputFile",
        dest="inputFile",
        help="The .txt file containing the list of ROOT files to submit jobs for (required)",
        default=None,
    )
    parser.add_argument(
        "-c",
        "--configFile",
        dest="configFile",
        help="The .txt file containing the base config settings for AnalysisTop (required)",
        default=None,
    )
    parser.add_argument(
        "-v",
        "--version",
        dest="version",
        help="The release version of AnalysisTop to use, e.g. 21.2.124 (required)",
        default=None,
    )
    cmdLineArgs = parser.parse_args()

    if cmdLineArgs.inputFile is None:
        print(
            "Please specify the .txt file containing the list of ROOT files to submit jobs for using --inputFile."
        )
        sys.exit(1)

    if cmdLineArgs.configFile is None:
        print(
            "Please specify the .txt file containing the base config settings for AnalysisTop using --configFile."
        )
        sys.exit(1)

    if cmdLineArgs.version is None:
        print(
            "Please specify the release version of AnalysisTop to use, e.g. 21.2.124, using --version"
        )
        sys.exit(1)

    command = (
        "mkdir "
        + dirPath
        + "/inputFiles "
        + dirPath
        + "/validation-cuts "
        + dirPath
        + "/executables "
        + dirPath
        + "/condorScripts "
        + dirPath
        + "/outputFiles "
        + dirPath
        + "/logs"
    )
    os.system(command)

    with open(cmdLineArgs.inputFile, "r") as inputFile:
        for index, line in enumerate(inputFile, 1):
            makeInputFiles(index, line, cmdLineArgs.configFile)
            makeExecutable(index, cmdLineArgs.version)
            makeCondorScript(index)
            submitCondorScript(index)


if __name__ == "__main__":
    main()
