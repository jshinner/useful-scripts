"""
This Python module contains functions to import a dataset from ROOT files, 
and format the dataset as a Pandas dataframe that is saved using the HDF5 storage format.
"""

import numpy as np
import pandas as pd
import uproot
import os
import sys
import glob
import argparse


class DataImporter(object):
    """A class defining the data importer for creating an HDF5 store of Pandas dataframes from a directory of .root files"""

    def __init__(self, storeName, directory, variables, overwriteIsEnabled):
        """
        Class parameters:
            storeName (str): The filepath of the store to be opened/created
            directory (str): The directory path containing the .root files
            variables (list of str): A list of the variables to be read from the .root tree, e.g. ["mu_pt", "mu_phi", ...]
            overwriteIsEnabled (bool): Boolean specifying whether to overwrite existing store
            
        """
        self.storeName = storeName
        self.directory = directory
        self.variables = variables
        self.overwriteIsEnabled = overwriteIsEnabled
        self.store = None

    def __enter__(self):
        # Delete existing store if overwrite option is enabled
        if self.overwriteIsEnabled:
            os.remove(self.storeName)
        # Open or create HDF5 store
        self.store = pd.HDFStore(self.storeName)
        return self

    def __exit__(self, type, value, tb):
        self.store.close()

    def getRootFilepaths(self):
        """
        Get a list of all the filepaths of .root files in a specified directory.

        Returns:
            listOfFiles: A list containing all the .root files in the directory
        """

        absDirectory = os.path.realpath(os.path.expanduser(self.directory))
        listOfFiles = glob.glob(absDirectory + "/*.root")
        listOfFiles.sort()
        return listOfFiles

    def getDataframeFromRootfile(self, filepath):
        """Get Pandas dataframe from .root file using uproot, and save it to the HDF5 store."""

        # Ignore files that are already stored

        filename = filepath.split("/")[-1]
        if "/IndividualFiles/%s" % filename.replace(".", "_") not in self.store.keys():
            print "Opening ROOT file %s" % filepath
            # Open the .root file and turn the nominal tree into a pandas dataframe
            nominalTree = uproot.open(filepath)["nominal"]
            df = nominalTree.pandas.df(self.variables)
            # Save the dataframe for the individual file
            df.to_hdf(
                self.store,
                key="IndividualFiles/%s" % filepath.split("/")[-1].replace(".", "_"),
                mode="a",
                format="table",
            )
            # Append to an overall dataframe
            self.store.append("df", df)
        else:
            print "A file named %s already has an entry in the store. Ignoring" % filename

    def getDataframesFromDirectory(self):
        """Get Pandas dataframe from all .root files in a directory, saving them to the HDF5 store."""
        listOfFiles = self.getRootFilepaths()
        for index, filepath in enumerate(listOfFiles):
            self.getDataframeFromRootfile(filepath)


def handleCommandLineArgs():
    """
    Handle the module's command line arguments when called directly, using the module argparse.

    Includes checks that the required arguments have been provided, and in the case that the
    argument --overwrite is provided, a confirmation that this is the intended behaviour.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--directory",
        dest="directory",
        help="The directory containing the ROOT files to be read (required)",
        default=None,
    )
    parser.add_argument(
        "--storeName",
        dest="storeName",
        help="The desired filepath for the HDF5 store (default 'store.h5')",
        default="store.h5",
    )
    parser.add_argument(
        "--variables",
        dest="variables",
        help="A list of the variables to be read from the ROOT files (required), e.g. 'mu_pt mu_phi'",
        nargs="*",
        default=None,
    )
    parser.add_argument(
        "--overwrite",
        dest="overwriteIsEnabled",
        help="A boolean specifying whether to overwrite the existing store, i.e. if changing the variables to be read",
        default=False,
        action="store_true",
    )
    cmdLineArgs = parser.parse_args()

    if cmdLineArgs.directory == None:
        print "No directory specified"
        sys.exit(1)

    if cmdLineArgs.variables == None:
        print "No variables specified to be read"
        sys.exit(1)

    if cmdLineArgs.overwriteIsEnabled:
        while True:
            overwriteConfirmation = raw_input(
                "--overwrite specified. Do you want to proceed? (y/n) \n"
            )
            yes = ["y", "yes", "Y", "Yes"]
            no = ["n", "no", "N", "No"]
            if overwriteConfirmation in yes:
                break
            elif overwriteConfirmation in no:
                sys.exit(1)
            else:
                print "Invalid input. Please specify 'yes' or 'no' (or 'y' or 'n')"

    return cmdLineArgs


def main():

    cmdLineArgs = handleCommandLineArgs()

    with DataImporter(
        cmdLineArgs.storeName,
        cmdLineArgs.directory,
        cmdLineArgs.variables,
        cmdLineArgs.overwriteIsEnabled,
    ) as importer:
        importer.getDataframesFromDirectory()


if __name__ == "__main__":
    main()
