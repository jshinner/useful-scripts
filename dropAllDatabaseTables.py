"""
Script providing a database connector class with functions to drop all of a user's 
tables from a database.

Takes one command line argument:
--conn: The connection string for the DB, in the form 'user/pw@dbsid' or 'user/pw'.
"""
import cx_Oracle
import argparse


class DatabaseConnector(object):
    def __init__(self, connString):
        self.dbuser = connString.split("/")[0]
        try:
            self.dbpw = connString.split("@")[0].split("/")[1]
        except IndexError:
            raise RuntimeError(
                "Invalid connection string. Should be 'user/pw@dbsid' or 'user/pw'"
            )
        try:
            self.dbsid = connString.split("@")[1]
        except IndexError:
            self.dbsid = None

    def __enter__(self):
        if self.dbsid is None:
            print "Opening DB connection %s" % self.dbuser
            self.connection = cx_Oracle.Connection("%s/%s" % (self.dbuser, self.dbpw))
        else:
            print "Opening DB connection %s@%s" % (self.dbuser, self.dbsid)
            self.connection = cx_Oracle.Connection(
                "%s/%s@%s" % (self.dbuser, self.dbpw, self.dbsid)
            )
        self.cursor = cx_Oracle.Cursor(self.connection)
        return self

    def __exit__(self, a, b, traceback):
        print "Closing DB connection"
        self.connection.close()

    def getAllTables(self):
        """Query the DB to fetch a list of all table names."""
        print "Fetching all table names"
        query = "SELECT TABLE_NAME FROM USER_TABLES"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        listOfTables = []
        if len(result) > 0:
            for tableTuple in result:
                listOfTables.append(tableTuple[0])
        else:
            print "No tables found in USER_TABLES"
        return listOfTables

    def getAllForeignKeyConstraints(self):
        """Query the DB to fetch a list of all foreign key constraints, that need to be dropped before the tables can be."""
        print "Fetching all FK constraints"
        query = "SELECT TABLE_NAME, CONSTRAINT_NAME FROM USER_CONSTRAINTS WHERE CONSTRAINT_TYPE='R'"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        if len(result) == 0:
            print "No FK constraints found in USER_CONSTRAINTS"
        return result

    def dropForeignKeyConstraint(self, fkTuple):
        """
        Drop a foreign key constraint.

        Input:
        fkTuple (tuple of length 2): Tuple in the form (tableName, fkConstraintName)
        """
        query = "ALTER TABLE %s DROP CONSTRAINT %s" % (fkTuple[0], fkTuple[1])
        self.cursor.execute(query)
        self.cursor.connection.commit

    def dropTable(self, tableName):
        """
        Drop a table.

        Input
        tableName (string): The name of the table to be dropped
        """
        query = "DROP TABLE %s PURGE" % tableName
        self.cursor.execute(query)
        self.cursor.connection.commit()

    def dropAllTables(self):
        """
        Clear all the tables in a database
        """

        # Get all table names and FK constraint names
        listOfTables = self.getAllTables()
        listOfForeignKeyConstraints = self.getAllForeignKeyConstraints()

        print "Dropping all FK constraints"
        for fkTuple in listOfForeignKeyConstraints:
            self.dropForeignKeyConstraint(fkTuple)

        print "Dropping all tables"
        for tableName in listOfTables:
            self.dropTable(tableName)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--conn",
        dest="connString",
        help="The connection string for the DB, in the form 'user/pw@dbsid' or 'user/pw' (required)",
        default=None,
    )
    cmdLineArgs = parser.parse_args()

    if cmdLineArgs.connString == None:
        raise RuntimeError(
            "No connection string provided. Please provide a connection string (via --conn) in the form 'user/pw@dbsid' or 'user/pw'"
        )

    with DatabaseConnector(cmdLineArgs.connString) as connector:
        connector.dropAllTables()


if __name__ == "__main__":
    main()
