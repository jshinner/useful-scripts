# useful-scripts

A repository containing a variety of miscellaneous scripts that serve various time-saving purposes for HEP-related work.

## `condorSubmitAnalysisTop.py`

A script that submits a batch of HTCondor jobs to use the ATLAS AnalysisTop framework 
to process a batch of ROOT files.

Takes three command line arguments:

* `-i`/`--inputFile`: .txt file containing a list of the ROOT files to be processed
* `-c`/`--configFile`: .txt file containing the config settings for AnalysisTop
* `-v`/`--version`: The release version of AnalysisTop to use, probably 21.2.X

## `dropAllDatabaseTables.py`

A script providing a database connector class with functions to drop all of a user's 
tables from a database.

Takes one command line argument:

* `--conn`: The connection string for the DB, in the form 'user/pw@dbsid' or 'user/pw'.

## `getTotalEvents.py`

A script to extract the total number of events contained in a set of ROOT files.

The script takes one argument `--directory` specifying the location of the ROOT files.

The script then opens all ROOT files in this directory and returns a print out of the total number of events in the file.

## `memoryMonitor.py`

A script that utilises the linux command `top` to check the memory usage of a Python process. Run this alongside another Python script to return the max. RAM, VRAM, and swap usage when the script exits.

## `rootfilesToDataframes.py`

A script to import a dataset from ROOT files, and format the dataset as a Pandas dataframe that is saved using the HDF5 storage format.

The script has three required arguments:

* `--directory`, specifying the location of the ROOT files to be read.
* `--storeName`, the filepath for the desired HDF5 store.
* `--variables`, the list of variables to be read from the ROOT file, e.g. 'mu_pt mu_phi'.

The script also has an additional optional argument `--overwrite` that allows an existing HDF5 store to be overwritten.
