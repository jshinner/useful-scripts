import glob
import os
import argparse
import ROOT


def getFilenames(directory):
    abs_directory = os.path.realpath(os.path.expanduser(directory))
    listOfFiles = glob.glob(abs_directory + "/*.root")
    return listOfFiles


def getFullRun2ScaledEventWeight(totalEventsWeighted, dsid):
    L = 139
    if dsid == "410470":
        sigma = 452.352426 * 1000
    elif dsid == "410557":
        sigma = 364.6715904 * 1000
    elif dsid == "410558":
        sigma = 87.7184 * 1000
    elif dsid == "410464":
        sigma = 366.2498025 * 1000
    elif dsid == "410465":
        sigma = 89.1447196 * 1000
    elif dsid == "410480":
        sigma = 364.7428388 * 10000
    elif dsid == "410482":
        sigma = 87.70054324 * 10000
    else:
        raise RuntimeError("Unrecognised DSID provided, cross section unkown")

    eventWeight = (L * sigma) / totalEventsWeighted
    return eventWeight


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--directory",
        dest="directory",
        help="The directory containing the ROOT files for which to calculate the total number of events.",
        default=None,
    )
    parser.add_argument(
        "--dsid",
        dest="dsid",
        help="The DSID of the dataset.",
        default=None,
    )
    cmdLineArgs = parser.parse_args()

    listOfFiles = getFilenames(cmdLineArgs.directory)
    i = 0
    totalEvents = 0
    totalEventsWeighted = 0

    for file in listOfFiles:
        i += 1
        print("Opening file %i" % i)
        ROOTfile = ROOT.TFile(file)
        tree = ROOTfile.sumWeights
        for entry in tree:
            totalEvents += entry.totalEvents
            totalEventsWeighted += entry.totalEventsWeighted

    eventWeight = getFullRun2ScaledEventWeight(totalEventsWeighted, cmdLineArgs.dsid)

    print("The directory contains %i files" % i)
    print("The total number of events is %i" % totalEvents)
    print("The total sumWeights is %e" % totalEventsWeighted)
    print("The event weight to scale to full run 2 is %f" % eventWeight)


if __name__ == "__main__":
    main()
