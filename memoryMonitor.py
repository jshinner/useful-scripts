import os
import time

runBool = True
maxVirt = 0
maxRam = 0
maxSram = 0

while runBool:
    user = os.environ["USER"]
    memoryMonitor = os.popen("top -n 1 -u %s" % user)
    time.sleep(3)
    countPython = 0
    for line in memoryMonitor.readlines():
        if "python" in line:
            countPython += 1
            processDetails = []
            for entry in line.replace(" ", ",").split(","):
                if entry != "":
                    processDetails.append(entry)

            i = processDetails.index("jshinner")

            virt = (
                processDetails[i + 3]
                .replace("m", "000")
                .replace("g", "00000")
                .replace(".", "")
            )
            if float(virt) > maxVirt:
                maxVirt = float(virt)

            ram = (
                processDetails[i + 4]
                .replace("m", "000")
                .replace("g", "00000")
                .replace(".", "")
            )
            if float(ram) > maxRam:
                maxRam = float(ram)

            sram = (
                processDetails[i + 5]
                .replace("m", "000")
                .replace("g", "00000")
                .replace(".", "")
            )
            if float(sram) > maxSram:
                maxSram = float(sram)

    if countPython < 2:
        runBool = False

print "Max virtual memory:", maxVirt / 1e6, "GB"
print "Max RAM:", maxRam / 1e6, "GB"
print "Max shared memory:", maxSram / 1000.0, "MB"
